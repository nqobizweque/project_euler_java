package com.company;

import com.sun.tools.javac.util.Pair;

import java.util.HashSet;

public class Solutions {

    protected static Integer Solution1(int max, int... nat_numbers) {
        String problem = "If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.\n" +
                "Find the sum of all the multiples of 3 or 5 below 1000.";
        HashSet<Integer> multiples = new HashSet<>();
        int[] nat_numbers_up = nat_numbers.clone();
        boolean hasAdd;
        do {
            hasAdd = false;
            for (int index = 0; index < nat_numbers.length; index++) {
                if (nat_numbers_up[index] < max) {
                    multiples.add(nat_numbers_up[index]);
                    nat_numbers_up[index] += nat_numbers[index];
                    hasAdd = true;
                }
            }
        } while (hasAdd);
        return multiples.stream().mapToInt(Integer::intValue).sum();
    }

    protected static Integer Solution2(int max){
        String problem = "Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be:\n" +
                "1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...\n" +
                "By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.";
        int value1 = 1, value2 = 2, sum = 0;
        while(value2 < max || value1 < max) {
            if (value1 < value2) {
                if(value2 % 2 == 0) {
                    sum += value2;
                }
                value1 += value2;
            } else {
                if(value1 % 2 == 0) {
                    sum += value1;
                }
                value2 += value1;
            }
        }
        return sum;
    }

    
}
