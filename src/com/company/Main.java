package com.company;

import com.sun.tools.javac.util.Pair;

public class Main {

    public static void main(String[] args) {
	    // write your code here
        Print(Solutions.Solution1(1000, 3, 5), 100);
        Print(Solutions.Solution2(4_000_000), 100);
    }

    public static <K, V> void Print(Pair<K, V> answer){
        System.out.println("\n\nProblem:\n" + answer.fst);
        System.out.println("Answer: " + answer.snd);
    }

    public static void Print(Integer returned, Integer expected) {
        System.out.printf("Expected: %d, Returned: %d\n", expected, returned);
    }
}
